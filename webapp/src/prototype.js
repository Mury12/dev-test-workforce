import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue';
import VueTheMask from 'vue-the-mask';
import VueSession from 'vue-session';
import PageTitle from './components/PageTitle/PageTitle.vue';
import Overlayer from './components/Overlayer/Overlayer.vue';
import VueScrollTo from 'vue-scrollto'

Vue
  .use(VueSession, { persist: true })
  .use(VueTheMask)
  .use(BootstrapVue)
  .use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
  })


import Axios from 'axios';

Vue.component('page-title', PageTitle);
Vue.component('overlayer', Overlayer);

Vue.prototype.$getMedia = media => {
  return require('@/assets/images/' + media);
}

Vue.prototype.$money = (amount, currency = 'BRL') => {
  switch (currency) {
    case 'BRL':
      return `R$ ${String(Number(amount).toFixed(2)).replace('.', ',')}`
    case 'COIN':
      return `${String(Number(amount).toFixed(2)).replace('.', ',')} Coins`
  }
}

Vue.prototype.$authenticated = () => {
  return Vue.prototype.$getSessionToken() ? true : false;
}

Vue.prototype.$setCookie = (name, value) => {
  document.cookie = `${name}=${value}`
}

Vue.prototype.$getCookie = (name) => {
  var c = document.cookie;
  var r = c.split(name)[1];
  if (r) {
    r = (r.split(';')[0]).split('=');
    return r[1];
  }
  return false
}

Vue.prototype.$unsetCookie = (name) => {
  document.cookie = `${name}=${null}`
}

Vue.prototype.$http = {
  get(url, options) {
    return Axios.get(url, options);
  },
  post(url, body, options) {
    return Axios.post(url, body, options);
  }
}

Axios.get('https://api.ipify.org?format=json').then(r => {
  Vue.$userIp = r.data.ip
});

export default Vue;


