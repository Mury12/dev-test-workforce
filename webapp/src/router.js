import Vue from './prototype';
import VueRouter from 'vue-router'
import Home from './_pages/Home/Home'
let routes = [
    /** Auth needed */
    {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: {
            protected: true,
            title: 'Home'
        }
    },
    {
        path: '*',
        component: Home,
        name: 'Other',
        meta: {
            title: 'Home'
        }
    }

]

Vue.use(VueRouter);

var router = new VueRouter({
    routes,
    mode: 'history'
});

export default router;