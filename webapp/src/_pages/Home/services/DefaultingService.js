import names from '../../../assets/util/names.json';

/**
 * Default class for API communication
 * 
 * ```js
 * 
 * const ctl = new DefaultingService();
 * const rows = await ctl.sync();
 * const filtered = await ctl.filter(filters);
 * const populate = await ctl.charge();
 * 
 * ```
 */
class DefaultingService {
    vm = null;
    constructor(vm) {
        if (!vm) throw Error("Expected VM not to be null.");
        this.vm = vm;
    }

    /**
     * Do a HTTP GET to the server in order to get unfiltered results
     * @returns array of results
     */
    async sync() {
        try {
            const result = await this.vm.$http.get(this.vm.$root.ws());
            return result.data;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /**
     * Do a HTTP POST to the server in order to insert 50 random data into the database
     * @returns array of results
     */
    async charge() {
        const data = this.randomData(50);
        try {
            const result = await this.vm.$http.post(this.vm.$root.ws(), data);
            return result;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /**
     * Do a HTTP GET to the server in order to get filtered results
     * @param {Object} filters row filters
     * @returns 
     */
    async filter(filters) {
        try {
            const query = this.parseFilters(filters);
            const result = await this.vm.$http.get(this.vm.$root.ws() + `?${query}`);
            return result.data;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /**
     * Parses the `filter` object into a query string
     * @param {Object} filters row filters
     * @returns 
     */
    parseFilters(filters) {
        let query = "";
        for (let item in filters) {
            if (filters[item] && filters[item].length) {
                if (item === 'fieldName' || item === 'query') {
                    if (filters.query && filters.query.length) {
                        query += `&${item}[]=${filters[item]}`;
                    }
                } else {
                    query += `&${item}=${filters[item]}`;
                }
            }
        }
        return query.substr(1, query.length);
    }

    /**
     * Generate random data to send to the api
     * @param {*} amount amout of items to insert
     * @returns {Array}
     */
    randomData(amount = 10) {
        const data = [];
        for (let i = 0; i < amount; i++) {
            data.push({
                customerName: this.randomName(),
                billetValue: (Math.random() * 100000).toFixed(2),
                date: this.randomDate(new Date(2010, 1, 1), new Date(), 0, 23)
            });
        }
        return data;
    }

    /**
     * Gets a random name from a list
     * @returns {string}
     */
    randomName() {
        const rand = Math.floor(Math.random() * names.length);
        return names[rand];
    }

    /**
     * Generate a random date between start and end dates.
     * @param {Date} start date start
     * @param {Date} end date end
     * @param {number} startHour hour start
     * @param {number} endHour hour end
     * @returns 
     */
    randomDate(start, end, startHour, endHour) {
        var date = new Date(+start + Math.random() * (end - start));
        var hour = startHour + Math.random() * (endHour - startHour) | 0;
        date.setHours(hour);
        return date;
    }
}

export default DefaultingService;