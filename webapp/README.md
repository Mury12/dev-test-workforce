# vue-start

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
Url de acesso será disposta no terminal, geralmente
`http://localhost:8080`
`http://x.x.x.x:8080`

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
