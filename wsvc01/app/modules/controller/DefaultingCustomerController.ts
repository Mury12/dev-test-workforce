import { IDefaultingTableFilters, IDefaultingTableItem } from "../interfaces/DefaultingTable";
import { respond } from "../util/respond";
import { tryCatch } from "../util/tryCatch";
import { MongoDBService } from "../services/MongoDB";
import { Collection } from "mongodb";
/**
 * This is the model controller class.
 * Do all the model's functions such as
 * authenticate, logout, CRUD functions
 * or processing.
 * 
 * @param {any} data model data 
 * 
 * @author Andre Mury
 * @version 0.0.1
 * 
 * ----
 * Example Usage
 * 
 * const uc = new UserController({username: 'John', password: 'm&69H13'});
 * 
 * if(await uc.login().success) {...}
 * 
 */
export class DefaultingCustomerController {
  private data = null as IDefaultingTableItem | Array<IDefaultingTableItem>;
  private table = "DefaultingTable" as string;
  private mongodb = null as MongoDBService;

  constructor(data?: IDefaultingTableItem | Array<IDefaultingTableItem>) {
    if (data) {
      this.data = data;
    }
    this.mongodb = new MongoDBService();
  }

  /**
   * Saves the item or array of items into de database
   * @returns 
   */
  async create(): Promise<any> {
    if (this.data) {
      try {
        /**
         * @var dbm Mongo DB connection instance
         */
        const dbm = await this.mongodb.connect();
        if (dbm) {
          /**
           * @var {Collection<any>} collection MongoDB collection instance
           */
          const collection = dbm.collection(this.table);

          /**
           * @var {any} result the MongoDB request results
           */
          let result = null;
          // Checks if data attribute is an array
          if (Array.isArray(this.data)) {
            result = await this.createMany(collection, this.data);
          } else {
            // If it isn't, convert this.data to array
            result = await this.createMany(collection, [this.data]);
          }
          // When done, colses database connection.
          this.mongodb.disconnect();

          return result;
        } else {
          throw new Error("Could not connect to the database.");
        }
      } catch (error) {
        throw error;
      }
    } else {
      throw new Error("Can't create an instance of item without item data.");
    }
  }

  /**
   * Inserts an array of items into the database
   * @param collection MongoDB Collection instance
   * @param data array of items
   * @returns 
   */
  protected createMany(collection: Collection<any>, data: Array<IDefaultingTableItem>): Promise<any> {
    const items = this.convertDate(data);
    return new Promise((resolve, reject) => {
      try {
        collection.insertMany(items, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        })
      } catch (error) {
        reject(error);
      }
    })
  }

  /**
   * Converts a date string to JavaScript Date Format.
   * @param data array of items
   * @returns 
   */
  protected convertDate(data: Array<IDefaultingTableItem>): Array<IDefaultingTableItem> {
    data.forEach((item) => {
      item.date = new Date(item.date);
    });
    return data;
  }

  /**
   * Gets a set of rows from the database
   */
  async get(filters?: IDefaultingTableFilters): Promise<Array<IDefaultingTableItem>> {
    try {
      const dbm = await this.mongodb.connect();
      if (dbm) {
        const collection = dbm.collection(this.table);
        let aggregation = {} as any;

        if (filters) {
          aggregation = this.parseFilters(filters);
        }

        const items = await collection.aggregate(aggregation).toArray();
        this.mongodb.disconnect();
        return items as Array<IDefaultingTableItem>;
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * Parses the filters attribute in order to obtain a valid MongoDB query object
   * @param filters 
   * @returns 
   */
  protected parseFilters(filters: IDefaultingTableFilters): Array<any> {
    const aggregation = [] as any;

    if (filters.orderBy.length)
      aggregation.push({
        '$sort': {
          [filters.orderBy]: filters.direction === 'DESC' ? -1 : 1,
        }
      });

    if (filters.filters.length) {
      const matches = [];
      filters.filters.forEach((item) => {
        matches.push({
          [item.fieldName]: new RegExp(item.query, 'igm')
        })
      });

      aggregation.push({
        '$match': {
          '$or': matches
        }
      })
    }

    if (filters.startAt) {
      aggregation.push({
        '$match': {
          date: {
            '$gte': filters.startAt
          }
        }
      })
    }

    aggregation.push({
      '$limit': filters.amount || 20
    })

    return aggregation;
  }

  /**
   * Returns the contents of `DefaultingCustomerController.data`
   * @returns 
   */
  getData(): Array<IDefaultingTableItem> | IDefaultingTableItem {
    return this.data;
  }

}