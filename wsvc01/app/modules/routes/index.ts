
// Routers

import { routes } from "./routes";

/**
 * This funciton prepares the routes to be instantiated
 * 
 * @param app fastify instance
 * @returns {Promise<any>} unresolved promise
 */
export async function router(app: any): Promise<any> {

    // Status check route
    app.get('/', function (req, res) {
        const { meh } = require('./app/modules/util/meh');
        let rand = Number(((Math.random() * meh.length) % (Number(meh.length) - 1)).toFixed(0));
        res.send({
            code: 200,
            status: meh[rand < 0 ? 0 : rand]
        });
    });

    return Promise.all(routes(app));
}