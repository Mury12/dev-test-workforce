import { FastifyReply, FastifyRequest } from "fastify";
import { DefaultingCustomerController } from "../../controller/DefaultingCustomerController";
import {  IDefaultingTableItem } from "../../interfaces/DefaultingTable";
import { parseQueryUrl } from "../../util/parse-query-url";
/**
 * GET one row from DB
 * @param {*} req 
 * @param {*} res 
 */
export const getOne = async (req: FastifyRequest, res: FastifyReply) => {
  res.send({ status: 'ok' });
}

/**
 * GET all rows from DB
 * @param {*} req 
 * @param {*} res 
 */
export const getAll = async (req: FastifyRequest, res: FastifyReply) => {
  const query = req.url.split('?')[1];

  const filters = parseQueryUrl(query);
  const ctl = new DefaultingCustomerController();
  const result = await ctl.get(filters);
  res.send(result);
}

/**
 * Inserts a new row into DB
 * @param {*} req 
 * @param {*} res 
 */
export const create = async (req: FastifyRequest, res: FastifyReply) => {
  const body = req.body as Array<IDefaultingTableItem> | IDefaultingTableItem;
  const ctl = new DefaultingCustomerController(body);

  const result = ctl.create();
  res.send(result);
}