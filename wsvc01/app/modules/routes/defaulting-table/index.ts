import { create, getAll, getOne } from './defaulting-table'

/**
 * Exports the users actions routes.
 * @param {*} router 
 * @param {*} options 
 */
export const defaultingTable = async (router: any, options: any) => {
  router.get('/', getAll);
  router.get('/:id', getOne);
  router.post('/', create);
}
