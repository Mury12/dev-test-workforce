import { defaultingTable } from "./defaulting-table";
import { log } from "./logger";

/**
 * Creates the array of routes to be set up.
 * 
 * @param app fastify instance
 * @returns {array<Promise>} array of promises
 */
export function routes(app: any) {
    // Register routes here
    return [
        app.register(log, { prefix: 'ws/v2/log' }),
        app.register(defaultingTable, { prefix: 'ws/v2/defaulting' }),
    ];
}