import { IFiltering } from "./Query";

/**
 * Interface for the defaulting table item
 * 
 * @property {string} customerName customer's name
 * @property {number} billetNumber billet's number
 * @property {number} billetValue billet's total value
 * @property {Date} date billet's register date
 */
export interface IDefaultingTableItem {
    _id?: string,
    customerName: string
    billetValue: number
    date: Date
}

/**
 * Interface for the defaulting table
 * 
 * @property {Array<IDefaultingTableItem>} items array of items to be displayed
 * @property {number} totalItems total number of items in the database
 * @property {number} amountShown amount of itens currently shown
 * @property {number} totalPages total number of pages for the selected amount of items
 * @property {number} currentPage current displayed page
 */
export interface IDefaultingTable {
    items: Array<IDefaultingTableItem>
    totalItems: number
    amountShown: number
    totalPages: number
    currentPage: number
}

/**
 * Interface for defaulting table filters
 * 
 * @property {Array<IOrdering>} order array of attributes to sort rows
 * @property {Array<IFiltering>} filters array of filters to apply when querying
 * @property {number} cursor target page to display
 * @property {number} amount number of rows to display
 */
export interface IDefaultingTableFilters {
    orderBy: string
    direction: string
    filters: Array<IFiltering>
    cursor: number
    amount: number
    startAt: Date
    [key: string]: any
}